<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function postComprobarReservas(Request $request)
    {

        $semanas = 1;
        $semana = 4 * 24 * 7;
        $dia = 4 * 24;
        $total = $semana * 3;

        $desde = "2022-06-14 12:30";
        $reservas = [];
        for ($i = 0; $i <= $dia; $i++) {
            $minute = $i * 15;
            $date = date('Y-m-j H:i', strtotime("+$minute minute", strtotime($desde)));

            $resultado = $this->comprobarFecha($date);
            if ($resultado["av"] == 1){
                echo $date . "<br>";
//                array_push($reservas, $date);
            }
        }

//        return view("reservas", [
//            "reserva" => $reservas
//        ]);
    }

    public function comprobarFecha($fecha)
    {
        $fecha = getdate(strtotime($fecha));

        $dia = $fecha['mon'] < 10 ? $fecha['mday'] . "-" . "0" . $fecha['mon'] . "-" . $fecha['year'] :
            $fecha['mday'] . "-" . $fecha['mon'] . "-" . $fecha['year'];

        $hora = $fecha['hours'] . "%253A" . $fecha['minutes'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://www.covermanager.com/reservation/update_people',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => "language=spanish&restaurant=restaurante-voltereta-nuevo&people=2&hour$hora&dia=$dia&only_this_people=&min_people=&max_people=8",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Accept: application/json, text/javascript, */*',
                'Cookie: ci_session=c9a116ec42d541e21e3d55732b6b271276a5033c'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response, true);;
    }

}
