<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        * {
            box-sizing: border-box;
        }

        form {
            width: 300px;
            padding: 16px;
            border-radius: 10px;
            margin: auto;
            background-color: #ccc;
        }

        form label {
            width: 72px;
            font-weight: bold;
            display: inline-block;
        }

        form input[type="text"],
        form input[type="email"] {
            width: 180px;
            padding: 3px 10px;
            border: 1px solid #f6f6f6;
            border-radius: 3px;
            background-color: #f6f6f6;
            margin: 8px 0;
            display: inline-block;
        }

        form input[type="submit"] {
            width: 100%;
            padding: 8px 16px;
            margin-top: 32px;
            border: 1px solid #000;
            border-radius: 5px;
            display: block;
            color: #fff;
            background-color: #000;
        }

        form input[type="submit"]:hover {
            cursor: pointer;
        }

        textarea {
            width: 100%;
            height: 100px;
            border: 1px solid #f6f6f6;
            border-radius: 3px;
            background-color: #f6f6f6;
            margin: 8px 0;
            /*resize: vertical | horizontal | none | both*/
            resize: none;
            display: block;
        }
    </style>

    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }

        select, input{
            width: 100%;
        }
    </style>
</head>
<body class="antialiased">
<h1>Calendario Voltereta</h1>
<form action="{{route('post.comprobar.reservas')}}" method="POST">
    @csrf
    @method('POST')

{{--    <p>Personas</p>--}}
{{--    <select name="personas" id="">--}}
{{--        @for($i=1;$i<=8;$i++)--}}
{{--            <option value="{{$i}}">{{$i}}</option>--}}
{{--        @endfor--}}
{{--    </select>--}}

{{--    <br>--}}
{{--    <p>Fecha</p>--}}
{{--    <input type="datetime-local" name="fecha"/>--}}
    <br>
    <input type="submit" value="Comprobar">
</form>

</body>
</html>
